Shallow and Optional Types
===

#### Abstract

Typed Racket (TR) is powerful --- but sometimes too powerful. In addition to a
type checker and type-driven optimizer, it includes a contract layer that
dynamically monitors interactions with untyped code. The contracts make TR one
of the strongest and most flexible type systems in the world, but also one
of the slowest when there are many boundaries to untyped code.

Shallow TR and Optional TR are two alternatives that have (finally!) arrived
with the Racket 8.7 release. Shallow TR enforces types with local assertions
rather than compositional contracts, keeping a bit of soundness at low cost.
Optional TR enforces types with nothing at all. This talk will explain Shallow
and Optional in depth and show how they can interact with untyped code,
standard TR, and each another.


#### Bio

Ben is currently a postdoc at Brown University studying human factors for type
systems and logics. Next Fall, he will be an assistant professor at the
University of Utah. Reach out if you would like to live on a mountain for N
years studying programming languages.


#### Outline

(see talk.ss)


#### bugs fixed with transient

https://github.com/racket/racket/issues/4421

```
  (define ((make-github-url kind) user repo name)
    (define url-str
      (format "https://github.com/~a/~a/~a/~a" user repo kind name))
    (define short-str
      (format "~a/~a #~a" user repo name))
    (hyperlink url-str (tt short-str)))
  (define github-issue (make-github-url "issues"))
  (define github-pull (make-github-url "pull"))

  \begin{tabular}{rlll}
       & kind   & merged? & pull request
  \\\hline
     1 & bugfix & \tblY & @github-pull["racket" "htdp" "98"]
  \\
     2 & bugfix & \tblY & @github-pull["racket" "pict" "60"]
  \\
     3 & bugfix & \tblY & @github-pull["racket" "racket" "3182"]
  \\
     4 & bugfix &  & @github-pull["racket" "typed-racket" "926"]
  \\
     5 & bugfix & \tblY & @github-pull["racket" "typed-racket" "919"]
  \\
     6 & bugfix & \tblY & @github-pull["racket" "typed-racket" "916"]
  \\
     7 & bugfix & \tblY & @github-pull["racket" "typed-racket" "914"]
  \\
     8 & bugfix & \tblY & @github-pull["racket" "typed-racket" "912"]
  \\
     9 & bugfix & \tblY & @github-pull["racket" "typed-racket" "923"]
  \\
    10 & bugfix & \tblY & @github-pull["racket" "typed-racket" "921"]
  \\
    11 & bugfix & \tblY & @github-pull["racket" "typed-racket" "918"]
  \\
    12 & bugfix & \tblY & @github-pull["racket" "typed-racket" "913"]
  \\
    13 & bugfix & \tblY & @github-pull["racket" "typed-racket" "884"]
  \\
    14 & bugfix & \tblY & @github-pull["racket" "typed-racket" "855"]
  \\
    15 & bugfix & \tblY & @github-pull["racket" "typed-racket" "612"]
  \\
    16 & bugfix & \tblY & @github-pull["racket" "typed-racket" "600"]
  \\
    17 & enhancement & \tblY & @github-pull["racket" "typed-racket" "927"]
  \\
    18 & enhancement & \tblY & @github-pull["racket" "typed-racket" "925"]
  \\
    19 & enhancement & \tblY & @github-pull["racket" "typed-racket" "911"]
  \\
    20 & enhancement & \tblY & @github-pull["racket" "typed-racket" "907"]
  \\
    21 & enhancement & & @github-pull["racket" "typed-racket" "917"]
  \end{tabular}
```




