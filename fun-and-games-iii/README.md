Summary of the Summer of #lang (Fun + Games III)
===

#### Abstract

Come learn about the amazing entries to this summer's `#lang` party!
Submissions include new languages, improved languages, language ideas, and
Standard ML. <https://github.com/lang-party/Summer2022>


#### Bio

Ben is a postdoc at Brown University and a co-organizer of the lang party with
Stephen DeGabrielle.


#### Outline

( ... )

