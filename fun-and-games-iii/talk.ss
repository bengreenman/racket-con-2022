#lang at-exp slideshow

;; Slides for SHORT presentation
;; Racket Con 2022
;;
;;  Summary of the Summer of #lang
;;  Fun and Games 3
;;
;; > Come learn about the amazing entries to this summer's `#lang` party!
;; > Submissions include new languages, improved languages, language ideas, and
;; > Standard ML. <https://github.com/lang-party/Summer2022>

;; TODO
;; - [X] welcome back
;; - [X] tower pict (early)
;; - [X] contributors, more space
;; - [X] lang-request
;; - [X] uniform format, move AWK and QI pictures, center tinybasic, rec-lang
;; - [X] eb screenshots https://gamefaqs.gamespot.com/snes/588301-earthbound/images?pid=588301&page=1
;;    ... decorate start/end?
;;    ... break up the langs?

;; - [X] basicc blocks for text, code, urls
;; - [X] better url block
;; - [X] code vs text
;; - [X] intro material
;; - [ ] use lingscars.com to inspire text lingscars.com
;; - [ ] lang request
;;   - idea: bring 2 groups together: lop curious & racket gurus
;;   - [ ] frosthaven
;;   - [X] text-adventure game
;;     - mapping out data structures
;;     - inspiration https://nanowrimo.org/
;; - [X] end material
;;   - [X] showcase
;;   - [X] contributor picts
;; - [X] write down idea for EVERY language: 1 liner, code, pic, ....
;;   - [X] russian-lang
;;   - [X] laundry
;;   - [X] Punct
;;   - [X] Beancount
;;     (huge docs behind the scenes: https://docs.google.com/document/d/1RaondTJCS_IUPBHFNdT8oqFKJjVJDsfsn6JEjBG04eA/edit)
;;   - [X] RAWK
;;     awk-like scripting language
;;   - [X] Hydromel
;;     hardware description language
;;     big example https://github.com/aumouvantsillage/Virgule-CPU
;;   - [X] recursive-language
;;     Chapter 6 of Computability and Logic (George Boolos, John P. Burgess, and Richard Jeffrey)
;;     ... executable math ... bringing textbook to life
;;   - [X] TinyBASIC
;;     https://blog.winny.tech/posts/lang-tinybasic/
;;     mental gymnastics
;;     code like this ... triangle
;;   - [X] F♭ m
;;     stack-based, designed by author Hypercubed
;;     for learning languages
;;     10 langs so far from racket to wolfram (show pics?)
;;     #lang reader ff /* factorial */ fact: dup 1 > [ dup 1 - fact * ] ? ; /* string printing */ (prints): dup [ q< (prints) q> putc ] ? ; prints: (prints) drop ; 0 'Factorial' 32 '100:' 10 prints 100 fact .
;;     https://github.com/Hypercubed/f-flat-minor
;;   - [ ] Budge
;;     esoteric lang
;;     https://esolangs.org/wiki/Budge-PL
;;     ex https://github.com/bor0/budge/blob/main/pl/src/budge-pl.ipynb
;;   - [X] Standard ML :
;;     (speaking of effort!)
;;     influential ... functional language
;;     not racket
;;         [ ] but racket does have mlish and hackett, in same neighborhood
;;   - [X] SML
;;     racket also has Leif's SML
;;     https://github.com/lang-party/Summer2022/issues/14
;;   - [X] Super
;;     lang extension: use anywhere #lang super racket etc (pict pict pict)
;;     obj.field (obj.method arg ...)
;;     [[ chained too! ]]
;;     id[expr] 
;;         [ ] before & after on adventofcode
;;   - [X] Sew
;;      - step toward literate programming
;;      - ... 8<-plan-from-here ... mysterious
;;      - normally boilerplate then interesting
;;      - before: (begin (provide main) (define (main) (displayln "Hello, world!")))
;;      - after: [8<-plan-from-here [<> ...] #'(begin (provide main) (define (main) <> ...))] (displayln "Hello, world!")
;;   - [X] gtp-output
;;      could benefit from Sew!
;;      language for a dataset, stylized format, makes sense to me!
;;      #lang = two purposes
;;      1. hook to explanation, name
;;      2. summary states
;;   - [X] tmux-vim-demo
;;     used by Qi tutorial
;;     #lang racket (require tmux-vim-demo) (run-demo "Qi" "." "start.rkt" "racket" #f)
;;     shell on left, vim on right, send to shell
;;     ... ok we can animate that
;;     #lang tmux-vim-demo #:name "python-demo" #:pre "python3" # this python program chooses 10 unique samples from 0 to 99 # and computes their average import random samples = random.sample(range(100), 10) sum(samples)/len(samples)
;;   - [X] Qi 3.0
;;     https://github.com/lang-party/Summer2022/issues/9
;;   - [X] Froglet
;;     Teaching language for formal methods, out of Brown
;;     BSL for Alloy (picture of dsj book)
;;         [ ] any code will do!
;;         [ ] check Shriram viz sheet for image
;;   - [X] CPSC411s
;;     family of compiler intermediate languages
;;     for nanopass compilers course by william at ubc
;;     scheme -> ...30 -> x86-64
;;     semantics along the way; base = umbrella
;;     #lang cpsc411/hashlangs/base (begin (set! r15 5) (set! r14 1) (with-label fact (compare r15 0)) (jump-if = end) (set! r14 (* r14 r15)) (set! r15 (+ r15 -1)) (jump fact) (with-label end (set! rax r14)) (jump done))
;;   - [X] GDLisp
;;     background = Godot gaming engine https://godotengine.org/
;;     compile to GDScript, use racket / macros beforehand
;;     #lang gdlisp (module macro racket (require gdlisp (for-syntax syntax/parse)) (provide swap!) (define-syntax (swap! stx) (syntax-parse stx [(_ a b) (with-syntax ([tmp (gensym "tmp")]) (syntax/loc stx (let ([tmp a]) (set! a b) (set! b tmp))))]))) (require 'macro) (define (foo a b c) (swap! a b) (match a [1 10] [2 20] [3 30] [_ (let ([acc 0]) (for ([n a]) (-set! acc 10)) acc)]))
;;     hey godot is gradually typed so they say https://docs.godotengine.org/en/latest/about/faq.html
;;   - [X] Karp :
;;          writing and testing NP reductions (you might remember from algo class)
;;          image = 3sat-to-indy.png
;;          2 languages
;;          (1) #lang karp/problem-definition (require karp/lib/cnf karp/lib/mapping) (decision-problem #:name 3sat #:instance ([𝜑 is-a (cnf #:arity 3)]) #:certificate (mapping #:from (variables-of 𝜑) #:to (the-set-of boolean))) ; 3-SAT verifier definition (define-3sat-verifier a c^a (∀ [c ∈ (clauses-of (𝜑 a))] (∃ [l ∈ (literals-of c)] (or (and (positive-literal? l) (c^a (underlying-var l))) (and (negative-literal? l) (not (c^a (underlying-var l))))))))
;;          (2) #lang karp/reduction (require "3sat.karp" "iset.karp" karp/lib/cnf karp/lib/graph karp/lib/mapping-reduction) (define-forward-instance-construction #:from 3sat #:to iset (3sat->iset a) (define Cs (clauses-of (𝜑 a))) ; create the set of vertices (Fig. 1 Step 1) (define V (for/set {(el l i) for [l ∈ C] for [(C #:index i) ∈ Cs]})) ; create a 1st set of edges (Fig. 1 Step 2) (define E1 (for/set {((el l1 i) . -e- . (el l2 j)) for [l1 ∈ (literals-of C1)] for [l2 ∈ (literals-of C2)] for [(C1 #:index i) ∈ Cs] for [(C2 #:index j) ∈ Cs] if (literal-neg-of? l1 l2)})) ; create a 2nd set of edges (Fig. 1 Step 3) (define E2 (for/set {((el (fst p) i) . -e- . (el (snd p) i)) for [p ∈ (all-pairs-in (literals-of C))] for [(C #:index i) ∈ Cs]})) (create-iset-instance ([G (create-graph V (set-∪ E1 E2))] [k (set-size Cs)])))
;;          generate examples to test your reduction!
;;          Rosette powered

;; Exciting, unsubmitted:
;; - wbowman compilers language ... headed to scheme workshop
;; - bennn scribble-tex

;; Inspiration:
;;  racket-con-2021/rcon2021.ss
;;  ~/code/postdoc/papers/ltl/vardifest22/talk.ss
;;  ~/code/postdoc/cif/2022-09-09/talk.ss

(require
  pict
  pict-abbrevs pict-abbrevs/slideshow
  ppict/2
  file/glob
  racket/draw
  racket/list
  racket/string
  racket/format
  racket/runtime-path
  scribble-abbrevs/pict)

(define-runtime-path img "./img")

(define turn revolution)

(define pico-x-sep (w%->pixels 1/100))
(define tiny-x-sep (w%->pixels 2/100))
(define border-x-sep (w%->pixels 4/100))
(define small-x-sep (w%->pixels 5/100))
(define smol-x-sep small-x-sep)
(define med-x-sep (w%->pixels 10/100))
(define big-x-sep (w%->pixels 15/100))

(define x%->pixels w%->pixels)
(define y%->pixels h%->pixels)

(define pico-y-sep (h%->pixels 1/100))
(define tiny-y-sep (h%->pixels 2/100))
(define small-y-sep (h%->pixels 5/100))
(define smol-y-sep small-y-sep)
(define med-y-sep (h%->pixels 10/100))
(define big-y-sep (h%->pixels 15/100))

(define code-line-sep (h%->pixels 8/1000))

(define slide-top 4/100)
(define slide-left 4/100)
(define slide-right (- 1 slide-left))
(define slide-bottom 86/100)
(define slide-text-left (* 3/2 slide-left))
(define slide-text-right (- 1 slide-text-left))
(define slide-heading-top (* 1.4 slide-top))
(define slide-text-top (* 6 slide-top))
(define slide-text-bottom slide-bottom)
(define lang-y 21/100)

(define text-coord (coord slide-text-left slide-text-top 'lt))
(define text-coord-l text-coord)
(define text-coord-m (coord 1/2 slide-text-top 'ct))
(define text-coord-r (coord slide-text-right slide-text-top 'rt))
(define heading-coord (coord slide-left slide-heading-top 'lt))
(define heading-coord-l heading-coord)
(define heading-coord-m (coord 1/2 slide-heading-top 'ct))
(define heading-coord-r (coord slide-right slide-heading-top 'rt))
(define bottom-coord-l (coord slide-left slide-text-bottom 'lb))
(define bottom-coord-m (coord 1/2 slide-text-bottom 'cc))
(define bottom-coord-r (coord slide-right slide-text-bottom 'rb))
(define center-coord (coord 1/2 1/2 'cc))
(define lang-coord-l (coord 46/100 lang-y 'rt))
(define lang-coord-m (coord 1/2 lang-y 'ct))
(define lang-coord-r (coord 54/100 lang-y 'lt))
(define lang-coord-lc (coord 46/100 46/100 'rc #:sep pico-y-sep))
(define lang-coord-rc (coord 54/100 46/100 'lc #:sep pico-y-sep))

(define (code-line-append* pp*)
  (apply vl-append code-line-sep pp*))

(define (code-line-append . pp*)
  (code-line-append* pp*))

;; COLOR
(define black (string->color% "black"))
(define gray (string->color% "light gray"))
(define white (string->color% "white"))
(define transparent (color%-update-alpha white 0))
(define mint-color (hex-triplet->color% #xcef7c3))
(define flag-blue (hex-triplet->color% #x6AB2E7))
(define flag-green (hex-triplet->color% #x12AD2B))
(define flag-red (hex-triplet->color% #xD7141A))
(define balrog-brown (hex-triplet->color% #xC3734E))
(define balrog-blue (hex-triplet->color% #x4090EF))
(define feilong-tan (hex-triplet->color% #xF0C090))
(define feilong-black (hex-triplet->color% #x565656))
(define vega-yellow (hex-triplet->color% #xDFF456))
(define vega-purple (hex-triplet->color% #x986EFB))
(define dhalsim-brown (hex-triplet->color% #xD09060))
(define url-blue (hex-triplet->color% #x1a0dab))
(define blanka-green (hex-triplet->color% #xC0F04F))
(define blanka-orange (hex-triplet->color% #xEF6101))
(define cammy-green (hex-triplet->color% #x85A564))
(define chunli-white (hex-triplet->color% #xD0F9FF))
(define ryu-white (hex-triplet->color% #xF0F0F0))
(define ken-red (hex-triplet->color% #xF06000))
(define guile-green (hex-triplet->color% #x50813F))
(define guile-yellow (hex-triplet->color% #xECF175))
(define guile-brown (hex-triplet->color% #xC08042))
(define honda-purple (hex-triplet->color% #x604F81))
(define honda-blue (hex-triplet->color% #x6DA2DC))
(define honda-red (hex-triplet->color% #xDD3A2D))
(define honda-white (hex-triplet->color% #xEBDAE3))
(define honda-litetan (hex-triplet->color% #xF1D293))
(define honda-darktan (hex-triplet->color% #xE19460))
(define honda-grey (hex-triplet->color% #x687598))
(define honda-black (hex-triplet->color% #x030105))
(define typed-color   (hex-triplet->color% #xF19C4D)) ;; orange
(define dark-green (hex-triplet->color% #x697F4D))
(define untyped-color honda-darktan)
(define natural-color honda-purple)
(define transient-color honda-blue)
(define deep-color natural-color)
(define shallow-color transient-color)
(define sand-color (hex-triplet->color% #xFFF7C2))
(define opt-orange (string->color% "Orange"))
(define opt-yellow (string->color% "cadet blue"))
(define opt-green (string->color% "MediumAquamarine"))

(define background-color feilong-tan)
(define deco-bg-color honda-grey #;honda-black)
(define dark-frame-color black)

(define flavor* (list
opt-orange
opt-green
honda-litetan
cammy-green
guile-yellow
vega-purple
balrog-blue
mint-color
flag-red
flag-blue
blanka-green
blanka-orange
guile-brown
vega-yellow
typed-color
honda-purple
feilong-tan
flag-green
))

(define (flavor->color n)
  (list-ref% flavor* n))

(define-values [next-flavor reset-flavors!]
  (let ((k (box 0)))
    (values
      (lambda ()
        (begin0 (unbox k) (set-box! k (add1 (unbox k)))))
      (lambda (n)
        (unless (exact-nonnegative-integer? n) (raise-argument-error 'reset-flavors! "natural?" n))
        (set-box! k n)))))

(define (list-ref% x* n)
  (list-ref x* (modulo n (length x*))))

(define racket-logo-mid
  (bitmap (build-path img "racket-med.png")))

(define title-font "Fourside")
(define code-font "Apple Kid")
(define text-font "Twoson" #;code-font #;"Marker Felt" #;"Iowan Old Style" #;"Kailasa")
(define subtitle-font text-font)
(define emph-font "Fourside")

(define title-size 38)
(define subtitle-size (- title-size 6))
(define body-size subtitle-size)
(define code-size 48)

(define ((make-string->text #:font font #:size size #:color color) . str*)
  (colorize (text (apply string-append str*) font size) color))

(define (make-string->title #:size [size title-size] #:color [color black])
  (make-string->text #:font title-font #:size size #:color color))

(define (make-string->subtitle #:size [size subtitle-size] #:color [color black])
  (make-string->text #:font subtitle-font #:size size #:color color))

(define (make-string->body #:size [size body-size] #:color [color black])
  (make-string->text #:font text-font #:size size #:color color))

(define (make-string->code #:size [size code-size] #:color [color black])
  (make-string->text #:font (cons 'bold code-font) #:size size #:color color))

(define (make-string->url #:size [size code-size])
  (make-string->text #:font (cons 'bold code-font) #:size size #:color url-blue))

(define ct (make-string->code))
(define urlt (make-string->url))
(define (ctdim . str*) (dim-cellophane (apply ct str*)))
(define titlet (make-string->title))
(define titlem (make-string->text #:size title-size #:font emph-font #:color black))
(define fourside titlem)
(define subtitlet (make-string->subtitle #:size body-size))
(define twoson subtitlet)
(define rt twoson)
(define t (make-string->body))
;(define urlt (make-string->code #:size (+ 4 code-size) #:color "medium blue"))

(define (dim-cellophane pp)
  (cellophane pp 3/10))

(define at "@")

(define (xblank x)
  (blank x 0))

(define (yblank x)
  (blank 0 x))

(define (ghid x)
  (ct (string-append at (~a x))))

(define (macroname x)
  (ct x))

(define (arrowhead-pict rad #:color [color black] #:size [size 20])
  (colorize
    (arrowhead size rad)
    color))

(define (up-arrow-pict)
  (arrowhead-pict (* 1/4 turn) #:color black))

(define (right-arrow-pict)
  (arrowhead-pict (* 0 turn) #:color black))

(define (down-arrow-pict)
  (arrowhead-pict (* 3/4 turn) #:color black))

(define (macroname2 x)
  (define (make-ptrim str)
    (if (for/and ((c (in-string x))) (< (char->integer c) 256))
      values
      (lambda (pp) (cb-superimpose (pblank @ct{X}) (clip-ascent pp)))))
  (define ptrim (make-ptrim x))
  (make-codeblock (ptrim (ct x))))

(define lang-name-pict macroname2)

(define (nice-frame pp)
  (make-codeblock pp))

(define niceframe nice-frame)

(define (nicecode* . code*)
  (nicecode code*))

(define (nicecode code*)
  (niceframe (apply ll-append code*)))

(define (lang-party-url)
  (niceframe @urlt{github.com/lang-party/Summer2022}))

(define (lang-party-logo [ww #f] [hh #f])
  (niceframe
    (tower-pict ww hh)))

(define (tower-pict [ww #f] [hh #f])
  (freeze
    (scale-to-fit
      (bitmap "img/logo.png")
      (or ww 200) (or hh 320))))

(define (imgframe pp)
  (make-codeblock #:x-margin 2 #:y-margin 2 pp))

(define (lbl-frame pp)
  (make-codeblock #:y-margin 6 pp))

(define lblframe lbl-frame)

(define (ctframe . str*)
  (nice-frame (apply ct str*)))

(define (titleframe . str*)
  (nice-frame (apply titlet str*)))

(define (orpp f x)
  (if (pict? x) x (f x)))

(define codeblock-radius 4)
(define codeblock-frame-width 2)

(define (make-codeblock xx #:x-margin [x-margin #f] #:y-margin [y-margin #f] #:flavor [n (next-flavor)] #:title [title #f])
  (let* ((code-pict
           (->code-pict xx))
         (block-pict
           (add-rounded-border
             #:x-margin (or x-margin border-x-sep)
             #:y-margin (or y-margin small-y-sep)
             #:radius codeblock-radius
             #:frame-color dark-frame-color
             #:frame-width codeblock-frame-width
             #:background-color white
             code-pict))
         (border-pict
           (add-rounded-border
             #:x-margin tiny-y-sep
             #:y-margin tiny-y-sep
             #:radius codeblock-radius
             #:frame-color dark-frame-color
             #:frame-width codeblock-frame-width
             #:background-color (flavor->color n)
             block-pict)))
    border-pict))

(define (nice-rect w h)
  (define c (flavor->color (next-flavor)))
  (filled-rounded-rectangle
    w h
    codeblock-radius
    #:color c
    #:border-color dark-frame-color
    #:border-width codeblock-frame-width))

(define (make-big-img str [w% 9/10] [h% 8/10])
  (scale-to-fit
    (bitmap (build-path img str))
    (w%->pixels w%)
    (h%->pixels h%)))

(define (->code-pict xx)
  (cond
    [(string? xx)
     (->code-pict (string-split xx "\n"))]
    [(list? xx)
     (code-line-append* (map (lambda (x) (if (string? x) (ct x) x)) xx))]
    [(pict? xx)
     xx]
    [else
      (raise-argument-error '->code-pict "(or/c string? pict? (listof (or/c string? pict?)))" xx)]))

(define (table2 kv**
                #:col-sep [pre-col-sep #f]
                #:row-sep [pre-row-sep #f]
                #:col-align [col-align lc-superimpose]
                #:row-align [row-align cc-superimpose])
  (define col-sep (or pre-col-sep tiny-x-sep))
  (define row-sep (or pre-row-sep tiny-y-sep))
  (table 2 (flatten kv**) col-align row-align col-sep row-sep))

(define tbl-arrange
  (lambda (x* render)
    (table 4 (map render (flatten x*)) cc-superimpose cc-superimpose pico-x-sep tiny-y-sep)))

(define stripe-arrange
  (lambda (x* render)
    (apply vc-append tiny-y-sep
           (map (lambda (y*) (apply hb-append pico-x-sep (map render y*))) x*))))

(define (rkterr str)
  (add-rectangle-background
    #:draw-border? #f
    #:x-margin tiny-x-sep
    #:y-margin tiny-y-sep
    (ct str)))

(define big-stripe-coord (coord 1/2 30/100 'ct))

(define (big-code-stripe pp)
  (make-codeblock
    (add-rectangle-background
      #:draw-border? #f
      #:color white
      #:x-margin (x%->pixels 1)
      #:y-margin med-y-sep
      pp)))

;(define results-pict (make-codeblock (hc-append small-x-sep @ct{Community Resource} @urlt{docs.racket-lang.org/syntax-parse-example})))
;(define submissions-pict (make-codeblock (hc-append small-x-sep @ct{Submissions} @urlt{github.com/syntax-objects/Summer2021})))
;(define contribute-pict (make-codeblock (hc-append small-x-sep @ct{Contribute! } @urlt{github.com/syntax-objects/syntax-parse-example})))

(define ((slide-assembler/background base-assembler make-rect) slide-title slide-vspace slide-pict)
  (define foreground-pict (base-assembler slide-title slide-vspace slide-pict))
  (define background-pict
    (let ((+margin (* 2 margin))
          (-margin (- margin)))
      (inset (make-rect (+ +margin client-w) (+ +margin client-h)) -margin)))
  (cc-superimpose background-pict foreground-pict))

(define dark-orange (hex-triplet->color% #xBF8232))
(define dark-red (hex-triplet->color% #x9D5969))
(define lite-green (hex-triplet->color% #x99CC99))
(define lite-blue (hex-triplet->color% #x6698CD))

(define checker-w 20)

(define (make-checker c)
  (filled-rectangle checker-w checker-w #:draw-border? #f #:color c))

(define (make-checkerboard w h c0 c1)
  (let* ((b0 (make-checker c0))
         (b1 (make-checker c1))
         (b01 (ht-append b0 b1))
         (b10 (ht-append b1 b0))
         (make-row (lambda (pp) (apply ht-append (make-list (+ 1 (quotient w (pict-width pp))) pp))))
         (row (vl-append (make-row b01) (make-row b10))))
    (apply vl-append (make-list (+ 1 (quotient h (pict-height row))) row))))

(define (clip-to w h pp)
  (clip
    (ppict-do
      (blank w h)
      #:go center-coord pp)))

(define (lite-checker w h) (clip-to w h (make-checkerboard w h lite-green lite-blue)))
(define (dark-checker w h) (clip-to w h (make-checkerboard w h dark-orange dark-red)))

(define (make-checkerboard2 w h c0 c1 c2 c3)
  (define board
    (let* ((b0 (make-checker c0))
           (b1 (make-checker c1))
           (b2 (make-checker c2))
           (b3 (make-checker c3))
           (b01 (ht-append b0 b1))
           (b10 (ht-append b1 b0))
           (b23 (ht-append b2 b3))
           (b32 (ht-append b3 b2))
           (make-row (lambda (pp0 pp1)
                       (ht-append
                         (apply ht-append (make-list (+ 1 (quotient (* 1/2 w) (pict-width pp0))) pp0))
                         (apply ht-append (make-list (+ 1 (quotient (* 1/2 w) (pict-width pp1))) pp1)))))
           (row (vl-append (make-row b01 b23) (make-row b10 b32))))
      (apply vl-append (make-list (+ 1 (quotient h (pict-height row))) row))))
  (clip
    (ppict-do
      (blank w h)
      #:go center-coord board)))

(define (half-checker w h)
  (let* ((w/2 (/ w 2))
         (L (make-checkerboard w/2 h lite-green lite-blue))
         (R (make-checkerboard w/2 h dark-orange dark-red))
         (board (hc-append L R)))
    (clip-to w h board)))

(define bg-orig (current-slide-assembler))
(define bg-lite (slide-assembler/background bg-orig lite-checker))
(define bg-half (slide-assembler/background bg-orig half-checker))
(define bg-dark (slide-assembler/background bg-orig dark-checker))

(define line-sep (+ 2))

(define (ll-append . pp*)
  (ll-append* pp*))

(define (ll-append* pp*)
  (apply vl-append line-sep pp*))

(define (lc-append . pp*)
  (lc-append* pp*))

(define (lc-append* pp*)
  (apply vc-append line-sep pp*))

(define (lr-append . pp*)
  (lr-append* pp*))

(define (lr-append* pp*)
  (apply vr-append line-sep pp*))

(define (labeled-pict p0 title [str #f])
  (label-below
    p0 
    (make-codeblock
      (let ((title-pict (t title)))
        (if str
          (vc-append tiny-y-sep title-pict (t str))
          title-pict)))))

(define (label-below p0 p1)
  (vc-append small-y-sep p0 p1))

(define (half-list x*)
  (split-at x* (quotient (length x*) 2)))

(define (tri-list x*)
  (define n/3 (quotient (length x*) 3))
  (define-values [ll xx] (split-at x* n/3))
  (define-values [cc rr] (split-at xx n/3))
  (values ll cc rr))

(define (scale-to-min* pp*)
  (define-values [ww hh]
    (for/fold ((ww #f) (hh #f))
              ((pp (in-list pp*)))
      (define w (pict-width pp))
      (define h (pict-height pp))
      (values (minf ww w) (minf hh h))))
  (for/list ((pp (in-list pp*)))
    (if (and (= (pict-width pp) ww)
             (= (pict-height pp) hh))
      pp
      (scale-to-fit pp ww hh))))

(define (minf a b)
  (if a (min a b) b))

(define contributor-name*
  '(Kalimehtar tgbugs otherjoel aumouvantsillage bor0 Hypercubed bennn winny-
    countvajhula dmacqueen benknoble soegaard LeifAndersen sorawee
    eutro xgqt hendrikboom3 FanC096 PanAeon wilbowma rocketnia
    REA1))

(define (contributor*)
  (let* ((pfx (build-path img "contributors"))
         (raw* (map (lambda (x) (bitmap (build-path pfx x)))
                    (sort
                      (directory-list pfx)
                      <
                      #:key (lambda (pp) (string->number (car (string-split (path->string pp) ".")))))))
         (pict* (scale-to-fit* raw* 80 80)))
    (for/list ((key (in-list contributor-name*))
               (pict (in-list pict*)))
      (list key pict))))

(define lang-name*
  (list
    (list "Beancount" "forge/bsl" "text-adventure game" "SML")
    (list "Qi 3.0" "RAWK" "GDLisp" "recursive-language" "Karp")
    (list "Super" "tmux-vim-demo" "Standard ML" "TinyBASIC")
    (list "gtp-output" "F♭ m" "Budge" "Hydromel" "Sew")
    (list "Punct" "laundry" "russian-lang" "CPSC411s")))

(define lang-name-coord
  (let ((num-lang (length (flatten lang-name*))))
    (lambda (ii)
      ;; TODO double-check end coord
      ;; old denom = (* 3/2 num-lang)
      (coord (+ slide-left (/ ii 26))
             slide-heading-top
             'lt))))

(define (scale-to-fit* pp* ww hh)
  (map (lambda (x) (scale-to-fit x ww hh)) pp*))

;;(define wide-codeblock-w (pict-width (before:define-extend)))
;;(define widen
;;  (let ((bb (blank wide-codeblock-w 0))) (lambda (pp) (cc-superimpose bb pp))))

(define (glob1 pat)
  (let ((mm (glob pat)))
    (cond
      ((null? mm) (raise-arguments-error 'glob1 "no matches" "pattern" pat))
      ((null? (cdr mm)) (car mm))
      (else (raise-arguments-error 'glob1 "N matches" "N" (length mm) "pat" pat "matches" mm)))))

(define (submit-pict)
  (define top-pict
        (niceframe
          (lc-append
            @rt{Submit any language}
            @ct{July - October})))
  (define bot-pict
        (niceframe
          (scale (bitmap (glob1 "img/new-issue-lo.*")) 9/10)))
  (vc-overlap top-pict bot-pict))

(define (prize-pict)
  (define top-pict
        (niceframe
          @rt{Win a Prize}))
  (define bot-pict
        (niceframe
          (cc-superimpose
            (xblank 260)
            (freeze (scale (inset/clip (bitmap "img/magnet.jpeg") -200 -180 -200 -600) 4/10))
            )))
  (vc-overlap top-pict bot-pict))

(define (vc-overlap top-pict bot-pict)
  (vc-append
    (ppict-do
      (pblank top-pict)
      #:go (coord 1/2 80/100 'ct) bot-pict
      #:go center-coord top-pict)
    (pblank bot-pict)))

(define (screenshot src)
  (define ww 200)
  (define hh ww)
  (if src
    (scale-to-fit (bitmap src) ww hh)
    (blank ww hh)))

(define (lop+guru-pict)
  (define lop
    (nice-frame
      (cc-superimpose
        (screenshot #f)
        (screenshot "img/fan.jpeg"))))
  (define guru
    (nice-frame
      (cc-superimpose
        (screenshot #f)
        (screenshot "img/guru.jpeg"))))
  (define txt
    (nice-frame @ct{LOP Fans   +   Racket Gurus}))
  (vc-append
    (* -7/10 (pict-height txt))
    (ht-append smol-x-sep lop guru)
    txt))

(define (ctlang . str*)
  (ct (apply string-append "#lang  " str*)))

(define (sammet-pict hh)
  (define tower
    (let ((pp (bitmap "img/sammet-acm.jpeg")))
      (niceframe (scale-to-fit pp (pict-width pp) hh))))
  (define jean
    (vc-append
      tiny-y-sep
      (niceframe
        (screenshot "img/sammet-head.jpeg"))
      (niceframe
        (lc-append
          @ct{Jean Sammet}
          @ct{(1928 - 2017)}))))
  (hc-append med-x-sep (tag-pict tower 'thetower) jean))

(define (book-pict hh)
  (define pp
   (freeze
    (inset/clip
      (bitmap "img/sammet-book.jpeg")
      -90  ;; L
      -70  ;; T
      -50  ;; R
      -500))) ;; B
  (scale-to-fit pp (pict-width pp) hh))

(define (nicert . arg*)
  (niceframe (apply rt arg*)))

(define (niceurlt . arg*)
  (niceframe (apply urlt arg*)))

(define (stdfish-pict)
  (standard-fish 200 100))

(define (quickscript-pict)
  (add-rounded-border
    #:frame-width 2
    #:x-margin pico-x-sep
    #:y-margin pico-x-sep
    (freeze (scale-to-fit (bitmap (build-path "img" "quickscript-competition-2020.png")) 200 200))))

(define (pillow-pict)
  (bitmap (build-path img "pillow.png")))

(define (pastevent pp)
  (define bg (blank (x%->pixels 38/100) (y%->pixels 28/100)))
  (niceframe
    (cc-superimpose bg pp)))

;; =============================================================================

(define (do-show)
  (set-page-numbers-visible! #false)
  (set-spotlight-style! #:size 60 #:color (color%-update-alpha highlight-brush-color 0.6))
  ;; --
  ;; bg-lite bg-half bg-dark
  (parameterize ((current-slide-assembler bg-lite))
    (sec:title)
    (sec:tradition)
    (sec:all)
    (sec:showcase)
    (sec:lang-request)
    (sec:links)
    (sec:thanks)
    (pslide)
    (sec:qa)

    (void))
  (void))

;; -----------------------------------------------------------------------------

(define title-coord (coord 1/2 26/100 'ct))

(define (title-block str [subt #f])
  (make-codeblock
    (add-rectangle-background
      #:x-margin med-x-sep
      #:y-margin 0
      #:draw-border? #f
      (let ((tt @titlet[str]))
        (if subt
          (vc-append tiny-y-sep tt @subtitlet[subt])
          tt)))))

(define (sec:title)
  (pslide
    ;; #:next
    #:go title-coord
    (title-block "Fun and Games 3" "Summary of the Summer of #lang")
    (blank 0 small-y-sep)
    (let ((pp (niceframe (vc-append tiny-y-sep @subtitlet{Ben  Greenman} @subtitlet{2022-10-30}))))
      (hc-append (pblank pp) pp))
  )
  (void))

(define (sec:tradition)
  (pslide
    ;; #:next
    #:go center-coord
    (niceframe @rt{Welcome Back!})
    ;; #:next
    #:go (coord 1/4 1/4 'cc)
    (pastevent (stdfish-pict)) ;; 2019
    #:go (coord 3/4 1/4 'cc)
    (pastevent (quickscript-pict)) ;; 2020
    #:go (coord 1/4 3/4 'cc)
    (pastevent (pillow-pict)) ;; 2021
    #:go (coord 3/4 3/4 'cc)
    (pastevent (tag-pict (blank) 'bbb))
    #:next
    #:go (at-find-pict 'bbb)
    (frame (tower-pict 200 200)) ;; 2022
  )
  (pslide
    ;; #:next
    #:go heading-coord-m
    (hc-append
      med-x-sep
      (lang-party-logo)
      (vc-append tiny-y-sep (submit-pict) (prize-pict)))
  )
  (void))

(define (sec:all)
  (pslide
    ;; #:next
    #:go center-coord
    (stripe-arrange lang-name* lang-name-pict)
  )
  (void))

(define (sec:showcase)
  ;; TODO unclear how to organize
  ;; - follow standard template for each?
  ;; - engagement comes first. most important thing. be fun and engaging, not boring listing
  ;; - lots of time/effort into these, really should present each one
  ;; TODO don't forget standard ml! fun way to break up the list
  (pslide
    ;; #:next
    #:go center-coord
    (niceframe @rt{Let's Go!})
  )
  (pslide
    ;; #:next
    #:go (lang-name-coord 0) (lang-name-pict "russian-lang")
    ;; #:next
    #:alt (
    #:go lang-coord-l
    (nicecode*
@ct|{#!1}|
@tt|{используется}|
(hb-append @tt|{  с-префиксом }| @ct|{r:  racket}|)
@ct|{}|
@ct|{r:letrec}|
@ct|{  ;}|
@ct|{    is-even? $ r:lambda (n)}|
@ct|{      n == 0 || is-odd? (n - 1)}|
@ct|{    is-odd? $ r:lambda (n)}|
@ct|{      n /= 0 && is-even? (n - 1)}|
@ct|{  is-odd? 11}|
)
    #:go lang-coord-rc
    (vc-append
      pico-y-sep
      @nicert{Russian Words}
      ;; @nicert{Infix Ops}
      @nicert{Sweet-Exprs})
)
    ;; https://docs.racket-lang.org/russian-lang/index.html
    ;; #:next
    #:go center-coord
    (niceframe
      (bitmap "img/russian-doc.png"))
    #:go bottom-coord-m
    @niceurlt{github.com/Kalimehtar/russian-lang}
  )
  (pslide
    ;; #:next
    #:go (lang-name-coord 1) (lang-name-pict "laundry")
    ;; #:next
    #:go lang-coord-lc
    (niceframe
      (lc-append
        @rt{Grammar For}
        @rt{Org-Mode}))
    #:go lang-coord-rc
    (nicecode*
@ctlang{org}
@ct{}
@ct|{#+todo: X Y | Z}|
@ct|{* DONE runtime keywords}|
@ct|{* FUTURE colorer}|
    )
    #:go bottom-coord-m
    @niceurlt{github.com/tgbugs/laundry}
  )
  (pslide
    ;; #:next
    #:go (lang-name-coord 2) (lang-name-pict "Punct")
    ;; #:next
    #:go lang-coord-lc
    (nicecode*
@ctlang{punct "my-tags.rkt"}
@ct|{---}|
@ct|{title: Prepare to be amazed}|
@ct|{date: 2020-05-07}|
@ct|{---}|
@ct|{> This is **markdown**.}|
@ct|{•attrib{Surly Buster (2008)}}|
)
    #:go lang-coord-rc
    (vc-append
      pico-y-sep
      (niceframe
        (lc-append
          @rt{Racket-Powered}
          @rt{Markdown}))
      (niceframe (hb-append @rt{Control Char } (clip-ascent (clip-descent @ct{•})))))
    #:go bottom-coord-m
    @niceurlt{github.com/otherjoel/punct}
  )
  (pslide
    ;; #:next
    #:go (lang-name-coord 3) (lang-name-pict "Beancount")
    ;; #:next
    #:go lang-coord-lc
    (niceframe
      (lc-append
        @rt{Double-Entry}
        @rt{Bookkeeping}))
    #:go lang-coord-rc
    (nicecode*
@ctlang{reader "beancount.rkt"}
@ct|{}|
@ct|{* Banking}|
@ct|{2020-01-01  open  Assets:BoA}|
@ct|{  institution: "Bank of America"}|
@ct|{}|
@ct|{2020-01-01 * "Opening Balance"}|
@ct|{  Assets:BoA         3239.66 USD}|
@ct|{  Equity:Opening    -3239.66 USD}|
@ct|{}|
@ct|{2020-01-06 * "Landlords Inc." "rent"}|
@ct|{  Assets:BoA        -2400.00 USD}|
@ct|{  Expenses:Rent      2400.00 USD}|
)
    #:go bottom-coord-m
    (let ((pp @niceurlt{github.com/PanAeon/beancount-racket}))
      (vc-append
        pp
        (yblank (- (* 7/10 (pict-height pp))))))
  )
  (pslide
    ;; #:next
    #:go (lang-name-coord 4) (lang-name-pict "RAWK")
    ;; #:next
    #:go lang-coord-l
    (nicecode*
@ctlang{rawk}
(yblank tiny-y-sep)
@ct|{BEGIN {}|
@ct|{    (print "First column:")}|
@ct|{    (define rows 0)}|
@ct|{}}|
(yblank tiny-y-sep)
@ct|{END { (print "Total  rows: " rows) }}|
(yblank tiny-y-sep)
@ct|{.* {}|
@ct|{    (print ($s 0))}|
@ct|{    (++ rows)}|
@ct|{}}|
)
    #:go lang-coord-rc
    @nicert{AWK Scripting}
    ;; #:next
    (niceframe
      (vc-append
        smol-y-sep
@ct|{transform "a,b,c\nd,e,f\ng,h,i\n"  ","}|
        (down-arrow-pict)
        (ll-append
@ct|{First column:}|
@ct|{a}|
@ct|{d}|
@ct|{g}|
@ct|{Total  rows: 3}|
)))
    #:go bottom-coord-m
    @niceurlt{gitlab.com/xgqt/racket-rawk/}
  )
  (pslide
    ;; #:next
    #:go (lang-name-coord 5) (lang-name-pict "Hydromel")
    ;; #:next
    #:go lang-coord-lc
    (vc-append
      pico-y-sep
      @nicert{Hardware Description}
      (nice-frame
        (lc-append
          @rt{Functional}
          @rt{+}
          @rt{Synth-able})))
    #:go lang-coord-rc
    (nicecode*
@ctlang{hydromel}
@ct|{}|
@ct|{component half_adder}|
@ct|{    port a : in  bit}|
@ct|{    port b : in  bit}|
@ct|{    port s : out bit}|
@ct|{    port c : out bit}|
@ct|{    c = a and b}|
@ct|{    s = a xor b}|
@ct|{end}|
)
    #:go bottom-coord-m
    @niceurlt{github.com/aumouvantsillage/Hydromel-lang}
  )
  (pslide
    ;; #:next
    #:go (lang-name-coord 6) (tag-pict (lang-name-pict "recursive-language") 'the-name)
    ;; #:next
    #:go lang-coord-lc
    (vc-append
      pico-y-sep
      @nicert{N -> N}
      (niceframe
        (lc-append
          @ct{Computability and Logic}
          (screenshot "img/computability-and-logic.jpg")
          ;; @ct{Boolos, Burgess, Jeffrey}
          )))
    #:go lang-coord-rc
    (nicecode*
@ctlang{recursive-language}
(yblank tiny-y-sep)
@ct|{import  Pr, Cn, s, id;}|
@ct|{}|
@ct|{sum = Pr[id_1^1, Cn[s, id_3^3]];}|
@ct|{}|
@ct|{check sum(2, 23) = 25;}|
)
    #:go bottom-coord-m
    @niceurlt{github.com/sorawee/recursive-language}
  )
  (pslide
    ;; #:next
    #:go (lang-name-coord 7) (lang-name-pict "TinyBASIC")
    ;; if you've seen basic, you know what's coming
    ;; if not ... real surprise
    ;; people really used to program like this!
    ;; #:next
    #:alt (
    #:go lang-coord-lc
    (tag-pict (nicecode*
@ctlang{tinybasic}
(yblank tiny-y-sep)
@ct|{....}|
@ct|{300 IF J = 1 THEN GOTO 682}|
@ct|{610 LET Z = 0}|
@ct|{612 LET Z = Z + 1}|
@ct|{615 LET X = A / 10}|
@ct|{620 IF X <> 0 THEN GOTO 612}|
) 'thecode)
    #:go lang-coord-rc
    @nicert{Experience History}
    )
    ;; #:next
    #:go center-coord
    (niceframe (scale (bitmap "img/pascal.png") 6/10))
    #:go bottom-coord-m
    @niceurlt{github.com/winny-/tinybasic.rkt}
  )
  (pslide
    ;; #:next
    #:go (lang-name-coord 8) (lang-name-pict "F♭ m")
    ;; #:next
    #:go lang-coord-lc
    (vc-append pico-y-sep
      @nicert{Stack-Based}
      @nicert{For Learning})
    #:go lang-coord-rc
    (nicecode*
@ctlang{reader ff}
(yblank tiny-y-sep)
@ct|{fact: dup 1 > [ dup 1 - fact * ] ? ;}|
(yblank tiny-y-sep)
@ct|{(prints): dup [ q< (prints) q> putc ] ? ;}|
@ct|{prints: (prints) drop ;}|
(yblank tiny-y-sep)
@ct|{0 'Factorial' 32 '100:' 10 prints}|
(yblank tiny-y-sep)
@ct|{5 fact .}|
@ct|{/* 120 */}|
)
    #:go bottom-coord-m
    @niceurlt{github.com/Hypercubed/f-flat-minor}
  )
  (pslide
    ;; #:next
    #:go (lang-name-coord 9) (lang-name-pict "Budge")
    ;; #:next
    #:alt (
    #:go lang-coord-lc
    (nicecode*
;      @ct{}
      @ct{[ [1, -1, 3, 5],}
      @ct{  [2, -2, 4, 6],}
      @ct{  [3, -3, -4],}
      @ct{  [6, -5, -6],}
      @ct{  [4, -4, 1, 3],}
      @ct{  [3, [3, -3], 2],}
      @ct{  [5, -5, 1]]}
;      @ct{[[2, -2, 1]]}
;      @ct{}
      )
    #:go lang-coord-rc
    (vc-append
      pico-y-sep
      @nicert{Godel numbering}
      @nicert{prime factorization})
    )
    ;; #:next
    #:go center-coord
    (niceframe
      (freeze (scale (bitmap "img/budge-wiki.png") 8/10)))
    #:go bottom-coord-m
    @niceurlt{esolangs.org/wiki/Budge-PL}
  )
  (pslide
    ;; JUST WHEN YOU THOUGHT IT COULDNT GET CRAZIER
    ;; #:next
    #:go (lang-name-coord 10) (lang-name-pict "Standard ML")
    ;; #:next
    #:go center-coord
    (vc-append
      tiny-y-sep
      @niceurlt{smlfamily.org}
      (nice-frame (screenshot "img/smlnj.jpeg")))
  )
  (pslide
    ;; #:next
    #:go (lang-name-coord 11) (lang-name-pict "SML")
    ;; #:next
    #:go lang-coord-lc
    (nicecode*
@ctlang{sml}
(yblank pico-y-sep)
@ct|{title: "A readme file"}|
@ct|{author: [}|
@ct|{  {name: "Leif"}|
@ct|{    location: {{Cambridge, @MA}}}}|
@ct|{  {name: "Ben"}|
@ct|{    location: {{Boston, @MA}}}]}|
(yblank tiny-y-sep)
@ct|{(define MA "Mass")}|
)
    #:go lang-coord-rc
    (vc-append pico-y-sep
      @nicert{Markup}
      @nicert{Not ML}
      ;; @nicert{150 Line Codebase}
    )
    ;; small codebase, well worth taking a look for engineering tips
    #:go bottom-coord-m
    @niceurlt{github.com/LeifAndersen/racket-sml}
  )
  (pslide
    ;; #:next
    #:go (lang-name-coord 12) (lang-name-pict "Super")
    ;; #:next
    #:go lang-coord-lc
    (vc-append
      pico-y-sep
      @nicert{Lang Extension}
      @nicert{Indexing}
      @nicert{Field + Method Access})
    #:go lang-coord-rc
    (nicecode*
@ctlang{super racket}
(yblank tiny-y-sep)
@ct|{(define str "abcde")}|
@ct|{str[1]}|
@ct{}
@ct|{(define p}|
@ct|{  (new point [x 0] [y 0]))}|
@ct|{(p .move-x 1)}|
    )
    #:go bottom-coord-m
    @niceurlt{github.com/soegaard/super}
  )
  (pslide
    ;; #:next
    ;; TODO try using for raco-pict slides
    ;; - step toward literate programming
    #:go (lang-name-coord 13) (lang-name-pict "Sew")
    ;; #:next
    #:go lang-coord-lc
    (nicecode*
@ctlang{sew racket}
@ct|{}|
@ct|{[8<-plan-from-here [<> ...]}|
@ct|{  #'(begin (provide main)}|
@ct|{           (define (main) <> ...))]}|
@ct{}
@ct|{(displayln "Hello, world!")}|
)
    #:go lang-coord-rc
    (vc-append
      pico-y-sep
      @nicert{Lang Extension}
      @nicert{Easy Editing}
)
    #:go bottom-coord-m
    @niceurlt{github.com/lathe/sew-for-racket}
  )
  (pslide
    ;; #:next
    #:go (lang-name-coord 14) (lang-name-pict "gtp-output")
    ;; #:next
    #:go lang-coord-lc
    (vc-append pico-y-sep @nicert{Data Description} @nicert{Quick Stats})
    #:go lang-coord-rc
    (ht-append (xblank (- smol-x-sep))
    (nicecode*
@ctlang{gtp-measure/output/typed-untyped}
(yblank pico-y-sep)
@ct|{("00000" ("cpu time: 566 ....}| ;;))
@ct|{("00001"  ("cpu time: 820 ....}| ;;))
@ct|{("00011"   ("cpu time: 805 ....}| ;;))
@ct|{....}|
))
    #:go bottom-coord-m
    @niceurlt{github.com/bennn/gtp-measure/blob/master/output/typed-untyped.rkt}
  )
  (pslide
    ;; TODO show the demo!
    ;; #:next
    #:go (lang-name-coord 15) (lang-name-pict "tmux-vim-demo")
    ;; #:next
    #:go lang-coord-lc
    (nicecode*
@ctlang{tmux-vim-demo}
(yblank tiny-y-sep)
@ct|{#:name "python-demo"}|
@ct|{#:pre "python3"}|
(yblank tiny-y-sep)
@ct|{# pick 10 samples, compute avg.}|
@ct|{from random import sample}|
@ct|{samples = sample(range(100), 10)}|
@ct|{sum(samples)/len(samples)}|
      )
    #:go lang-coord-rc
    (vc-append
      pico-y-sep
      @nicert{Interactive Demos}
      @nicert{Worry Free})
    #:go bottom-coord-m
    @niceurlt{github.com/benknoble/tmux-vim-demo}
  )
  (pslide
    ;; #:next
    #:go (lang-name-coord 16) (lang-name-pict "Qi 3.0")
    ;; #:next
    #:go lang-coord-lc
    (vc-append
      pico-y-sep
      ;; @nicert{Functional}
      (nice-frame (freeze (scale (bitmap "img/qi.png") 6/10)))
      @nicert{Flow-Oriented})
    #:go lang-coord-rc
    (nicecode*
@ctlang{racket}
@ct|{(require qi)}|
@ct|{(map  (☯ (_ 10 2))}|
@ct|{              (list + - * /))}|
(yblank pico-y-sep)
@ct|{;; (12  8  20  5)}|
(yblank pico-y-sep)
    )
    #:go bottom-coord-m
    @niceurlt{github.com/countvajhula/qi}
  )
  (pslide
    ;; #:next
    #:go (lang-name-coord 17) (lang-name-pict "forge/bsl")
    ;; #:next
    #:go lang-coord-lc
    (nicecode*
@ctlang{forge/bsl}
(yblank pico-y-sep)
; @ct|{one sig Init, Mid, End extends State {}}|
; (yblank pico-y-sep)
@ct|{sig  StackElem {}|
@ct|{  prev:   lone  StackElem}|
@ct|{}}|
(yblank tiny-y-sep)
@ct|{pred  Step1Pop {}|
@ct|{  some  Init.top}|
@ct|{  Mid.top = Init.top.prev}|
@ct|{}}|
)
    #:go lang-coord-rc
    @nicert{Teaching SW Modeling}
    #:go bottom-coord-m
    @niceurlt{github.com/tnelson/Forge}
  )
  (pslide
    ;; #:next
    #:go (lang-name-coord 18) (lang-name-pict "CPSC411 langs")
    ;; #:next
    #:go lang-coord-lc
    (nicecode*
@ctlang{cpsc411/hashlangs/base}
(yblank pico-y-sep)
@ct|{(begin}|
@ct|{  (set! r15 5)}|
@ct|{  (set! r14 1)}|
@ct|{  (with-label fact (compare r15 0))}|
@ct|{  (jump-if = end)}|
@ct|{  (set! r14 (* r14 r15))}|
@ct|{  (set! r15 (+ r15 -1))}|
@ct|{  (jump fact)}|
@ct|{  (with-label end (set! rax r14))}|
@ct|{  (jump done))}|
)
    #:go lang-coord-rc
    @nicert{126 Languages}
    @nicert{Scheme → x86}
    #:go bottom-coord-m
    @niceurlt{github.com/cpsc411/cpsc411-pub}
  )
  (pslide
    ;; #:next
    #:go (lang-name-coord 19) (lang-name-pict "GDLisp")
    ;; #:next
    #:go lang-coord-lc
    (vc-append
      pico-y-sep
      (niceframe (freeze (scale (bitmap "img/godot-logo.png") 4/10)))
      @nicert{GDScript})
    #:go lang-coord-rc
    (nicecode*
@ctlang{gdlisp}
(yblank tiny-y-sep)
@ct|{(define (foo a)}|
@ct|{  (match a}|
@ct|{    [1  10]}|
@ct|{    [2  20]}|
@ct|{    [_ (let ([acc 0])}|
@ct|{           (for ([n a])  (-set! acc 10))}|
@ct|{           acc)]))}|
)
    #:go bottom-coord-m
    @niceurlt{github.com/eutro/gdlisp}
  )
  (pslide
    ;; #:next
    #:go (lang-name-coord 20) (lang-name-pict "Karp")
    ;; #:next
    #:go lang-coord-lc
    (nicecode*
@ctlang{karp/problem-definition}
; @ct|{(require karp/lib/graph)}|
(yblank pico-y-sep)
@ct|{(decision-problem}|
@ct|{  #:name  iset}|
@ct|{  #:instance}|
@ct|{    ((G  is-a  (graph #:undirected))}|
@ct|{      (k  is-a  (natural)))}|
@ct|{  #:certificate}|
@ct|{    (subset-of (vertices-of G)))}|
(yblank pico-y-sep)
; @ct|{(define-iset-verifier y c^y}|
; @ct|{  (and}|
; @ct|{   (>= (set-size c^y) (k y))}|
; @ct|{   (∀ [u ∈ c^y]}|
; @ct|{      (∀ [v ∈ c^y]}|
; @ct|{         (set-∉ u (neighbors (G y) v))))))}|
)
    #:go lang-coord-rc
    (vc-append
      pico-y-sep
      @nicert{NP Reduction}
      @nicert{Random Testing})
    #:go bottom-coord-m
    @niceurlt{github.com/REA1/karp}
  )
  (void))

(define (sec:lang-request)
  (pslide
    ;; #:next
    #:go center-coord
    (niceframe @rt{That's all!})
  )
  (pslide
    ;; TODO screenshot
    ;; TODO call out d.ben + hendrik?
    ;; #:next
    #:go heading-coord-m
    (niceframe @rt{Part 2: Lang Request})
    (yblank smol-y-sep)
    ;; #:next
    #:alt ( (lop+guru-pict) )
    (yblank smol-y-sep)
    ;; #:next
    (tag-pict (niceframe @ct{1.  Text Adventure Game}) 'ttt)
    ;; #:next
    #:go (at-find-pict 'ttt lb-find 'lt #:abs-y tiny-y-sep)
    (niceframe @ct{2.  Frosthaven:  Monsters and Scenarios})
  )
  (void))

(define (sec:links)
  (pslide
    ;; #:next
    #:go center-coord
    (let ((txt (lang-party-url)))
      (ppict-do
        (lang-party-logo 300 400)
        #:go (coord 1/2 40/100 'cc)
        txt))
  )
  (pslide
    ;; #:next
    #:go center-coord
    (niceframe
      (hc-append @ct{Search   } @rt{H:} @ct{   for hashlangs}))
    (yblank tiny-y-sep)
    (niceframe
      (bitmap "img/lang-search.png"))
  )
  (void))

(define (sec:thanks)
  ;; TODO entries worldwide, prizes 50/50 domestic abroad
  (pslide
    ;; #:next
    #:go title-coord (title-block "Thank You!")
    (yblank small-y-sep)
    (let* ((stephen (bitmap (glob1 (build-path img "stephen.*"))))
           (cc* (for/list ((kv (in-list (contributor*)))
                           (ii (in-naturals)))
                  (ppict-do (cadr kv)
                            #:go (if (even? ii) (coord 1/2 98/100 'ct) (coord 1/2 0/100 'cb))
                            (rt (~a (car kv))))))
           (cc
               (scale
                 (let-values (((l* c* r*) (tri-list cc*)))
                   (apply vc-append
                     (+ smol-y-sep med-y-sep)
                     (map (lambda (pp*) (apply ht-append (w%->pixels 7/100) pp*))
                          (list l* c* r*))))
                 1/2)))
      (ht-append
        small-x-sep
        (make-codeblock (label-below cc @t{Contributors}))
        (make-codeblock (label-below stephen @t{Stephen}))))
    )
  (void))

(define (sec:qa)
  ;; why did lang-request fail?
  ;;  1. unclear from advert, only 1-2 lines
  ;;  2. submissions not advertised like the first langs were
  ;;  3. people are busy! hard to schedule N-way over summer
  (pslide
    #:go heading-coord-m
    (niceframe @rt{Image Credit})
    (yblank smol-y-sep)
    ;; #:alt ( (lang-party-logo 350 450) )
    ;; #:next
    (sammet-pict 450)
    #:next
    #:go (at-find-pict 'thetower cc-find 'cc)
    (niceframe (book-pict 450))
  )
;  (pslide
;    #:go center-coord
;    (stripe-arrange lang-name* lang-name-pict)
;  )
  (pslide
    )
  (void))

;; =============================================================================

(module+ main
  (do-show))

;; =============================================================================

(module+ raco-pict (provide raco-pict)
         ;;(define client-w 984) (define client-h 728)
         (define client-w 1320) (define client-h 726)
         (define raco-pict
  (ppict-do (lite-checker client-w client-h)

    #:go center-coord
    (niceframe
      (scale (bitmap "img/budge-wiki.png") 8/10))
    #:go bottom-coord-m
    @niceurlt{esolangs.org/wiki/Budge-PL}

  )))
